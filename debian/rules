#!/usr/bin/make -f

DESTDIR    := $(CURDIR)/debian/plm
ICONS      := $(DESTDIR)/usr/share/icons/hicolor


export JAVA_HOME=/usr/lib/jvm/default-java
depspath=/usr/share/java/miglayout.jar:/usr/share/java/rsyntaxtextarea.jar:/usr/share/java/gettext-commons.jar:/usr/share/java/junit4.jar
gitpath=/usr/share/java/org.eclipse.jgit.jar
scalapath=.
#scalapath=/usr/share/java/scala-compiler.jar:/usr/share/java/scala-library.jar
langpath=/usr/share/java/json-simple.jar:/usr/share/java/jython.jar:.
httppath=/usr/share/java/httpclient.jar:/usr/share/java/httpcore.jar:/usr/share/java/commons-logging.jar:/usr/share/java/httpmime.jar
export CLASSPATH=$(depspath):$(gitpath):$(langpath):$(httppath):$(scalapath):.

JSON_SIMPLE_VERSION = $(shell dpkg -l libjson-simple-java | grep '^ii' | awk '{print $$3}')
JSON_SIMPLE_3 = $(shell dpkg --compare-versions '$(JSON_SIMPLE_VERSION)' '>' '3.1.1-1~' && echo yes || echo no)
ifeq (yes,$(JSON_SIMPLE_3))
JSON_SIMPLE_PACKAGE = com.github.cliftonlabs.json_simple
JSON_EXCEPTION = JsonException
else
JSON_SIMPLE_PACKAGE = org.json.simple
JSON_EXCEPTION = DeserializationException
endif

%:
	dh $@   --with javahelper

execute_before_dh_auto_configure:
	# Making backups of files that will be altered during the build
	for F in $$(find l10n/engine -name "*.po" -o -name "*.pot") lib/resources/plm.configuration.properties; do \
	    cp $$F $${F}.save ;\
	done

override_dh_auto_clean:
	dh_auto_clean
	find . -type f -name \*.java.json-simple \
	  -exec sh -c 'file={} && mv $$file $${file%.json-simple}' \; -print

override_dh_clean:
	dh_clean
	# Removing files left there by the build system.
	find . -name "*.jar" -delete
	-rm dist/*.tar.bz2
	-rm errors-*.txt
	# Restoring files that were altered during the build
	for F in $$(find . -name "*.save") ; do \
	    mv $$F $${F%.save} ;\
	done

override_dh_auto_build:
	find . -type f -name \*.java -exec grep -q 'import @JSON_SIMPLE_PACKAGE@' {} \; \
	  -exec sed -i.json-simple \
		    -e 's,@JSON_SIMPLE_PACKAGE@,$(JSON_SIMPLE_PACKAGE),' \
		    -e 's/@JSON_EXCEPTION@/$(JSON_EXCEPTION)/' \
		    {} \; -print
	dh_auto_build

override_dh_auto_install:
	dh_install
	
	# Icons
	for i in 16 22 48; do \
	    mkdir -p $(ICONS)/$${i}x$${i}/apps; \
	    convert debian/logo.png -colorspace RGB \
	            -resize $${i}x$${i} -colorspace sRGB \
	            +set date:create +set date:modify -define png:exclude-chunk=time \
	            $(ICONS)/$${i}x$${i}/apps/plm.png; \
	done
	install -d -m 755 $(DESTDIR)/usr/share/pixmaps
	convert img/logo.png -colorspace RGB \
	        -resize 32x32 -colorspace sRGB \
		$(DESTDIR)/usr/share/pixmaps/plm.xpm
		
	# desktop
	install -D -m 644 debian/plm.desktop $(DESTDIR)/usr/share/applications/plm.desktop
		
	# wrapper script
	install -d -m 755 $(DESTDIR)/usr/bin
	cp debian/plm.sh $(DESTDIR)/usr/bin/plm
	chmod 755 $(DESTDIR)/usr/bin/plm

version=$(shell uscan --report-status | head -2 | tail -1 | cut -d' ' -f7 | cut -d, -f1)
tmpdir:=$(shell mktemp -d)
wrkdir:=$(shell pwd)
